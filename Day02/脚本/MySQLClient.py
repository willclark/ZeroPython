"""
测试通过
"""
from pathlib import Path
import os
from pprint import pprint
import configparser
import MySQLdb

config_filename = "dbMysqlConfig.cnf"
section_name = 'dbMysql'

this_folder = Path(__file__).parent
config_path = Path.joinpath(this_folder, config_filename)


def get_database(cfg_path, sec_name):
    config = configparser.ConfigParser()
    config.read(cfg_path)

    host = config.get(sec_name, 'host')
    port = config.getint(sec_name, 'port')
    user = config.get(sec_name, 'user')
    password = config.get(sec_name, 'password')
    db_name = config.get(sec_name, 'db_name')

    database = MySQLdb.connect(
        host=host,
        port=port,
        user=user,
        passwd=password,
        db=db_name,
        charset='utf8'
    )
    return database


db = get_database(config_path, section_name)

cursor = db.cursor()

# 使用 execute()  方法执行 SQL 查询
cursor.execute("SELECT * FROM city")

# 使用 fetchall() 方法获取所有数据.
data = cursor.fetchall()
pprint(data)

cursor.close()
db.close()
